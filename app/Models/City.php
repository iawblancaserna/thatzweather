<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'current_temp',
        'zip_code',
        'description',
        'icon'
    ];

    function scopeByMinTemp($query) {
        return $query->orderBy('current_temp','asc');
    }
}
