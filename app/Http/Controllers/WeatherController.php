<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use Illuminate\Support\Facades\Http;

use Illuminate\Http\Request;
use App\Models\City;

class WeatherController extends Controller
{
    //

    public function search(SearchRequest $request) {

        // Agafem el codi introduit per l'usuari
        $codi = $request->input('codi_postal');

        $response = Http::get('https://api.openweathermap.org/data/2.5/forecast', [
            'zip' =>  $codi . ',ES',
            'appid' => config('app.app_id'),
            'units' => 'metric',
            'lang' => 'es'
        ]);

        // Si hi ha un error quan crida la api
        if (!$response->successful()) {
            return redirect()->back()->withErrors(['error' => 'No se ha encontrado este código postal.']);
        }
        // dd($response['list']);
        $temp = round($response['list'][0]['main']['temp']); // temperatura actual
        $name = $response['city']['name']; // nom ciutat
        $desc = $response['list'][0]['weather'][0]['description']; // descripcio del clima
        $icon = $response['list'][0]['weather'][0]['icon']; // icon weather 

        // Creem un registre a la bd de la ciutat buscada
        // Si ja esta creada s'actualitza el registre 
        $city = City::updateOrCreate(
            ['zip_code' => $codi],

           ['name' => $name,
            'current_temp' => $temp,
            'zip_code' => $codi,
            'description' => $desc,
            'icon' => $icon
        ]);

        // Agafem dades de les proximes 4 hores per mostrar-les també
        for ($i = 1; $i < 5; $i++) {

            // Tallem el timestamp per agafar només l'hora exacta
            $timestamp = $response['list'][$i]['dt_txt']; // timestamp
            $timestamp = explode(" ", $timestamp);
            $timestamp = substr($timestamp[1], 0, 5);

            $proximes_hores[$i] = [
                "descripcion" => $response['list'][$i]['weather'][0]['description'],
                "temperatura_actual" => round($response['list'][$i]['main']['temp']),
                "icon" => $response['list'][$i]['weather'][0]['icon'],
                "hora" => $timestamp
            ];
        }

        $dias = ['', 'Lunes','Martes','Miercoles','Jueves','Viernes','Sabado', 'Domingo'];
       
        // dd($fecha);
        $aux = 0;
        // Agafem dades dels proxims 4 dies
        for ($j = 5; $j < 40; $j++) {

            // Agafem el dia per saber quin dia de la setmana es
            $timestamp = $response['list'][$j]['dt_txt']; // timestamp
            $fecha = $dias[date('N', strtotime($timestamp))]; // day of week

            $proxims_dies[$aux] = [
                "dia" => $fecha,
                "descripcion" => $response['list'][$j]['weather'][0]['description'],
                "temperatura_actual" => round($response['list'][$j]['main']['temp']),
                "icon" => $response['list'][$j]['weather'][0]['icon']
            ];

            $j += 7; // Cada 7 per anar canviant de dia
            $aux++; // var per assignar pos en l'array
        }
        // Per fer el top 5 agafem la informació de la bd
        $ciutats_top = City::byMinTemp()->take(5)->get();
        
        // dd($proximament);
        return view('results')->with('ciutat', $city)
            ->with('codi_postal', $codi)
            ->with('proximes_hores', $proximes_hores)
            ->with('proxims_dies', $proxims_dies)
            ->with('ciutats_top', $ciutats_top);
    }
}
