<!doctype html>
<html>
<head>
    <title> ThatzWeather </title>
    <link rel="shortcut icon" href="{{ asset('image/favicon.png') }}">
    <!-- CSS only -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Bootstrap links -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    {{-- muli font --}}
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet'>

    <style>
    </style>
</head>
<body>
    @yield('content') 
</body>
</html>