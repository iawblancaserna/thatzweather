@extends('layouts.header')
@section('content')
    <div class="container-fluid bg-img w-100 mh-100">
        <div class="d-flex justify-content-center pt-5 pb-5">
            <img src="{{ URL::asset('/images/logo.png') }}" alt="logo thatz">
        </div> <br> <br>
        <div class="container font-weight-bold">
            <div class="row d-flex justify-content-center ">
                <div class="col-6">

                    <p id='bold' class="text-center text-light">Entérate del tiempo en la zona exacta
                        que te interesa buscando por código postal</p>

                    <form action="{{ Route('search.post') }}" class="p-3" method="post">
                        @csrf

                        <div class="row">
                            <input class="form-control transparent-input" type="number" name="codi_postal" id="codi_postal"
                                placeholder="Introduce el código postal">
                        </div> <br>
                        @error('codi_postal')
                            <div class="alert alert-danger"> {{ $message }} </div>
                        @enderror
                        <div class="row">
                            <button id='bold' type="submit" class="btn btn-info text-light">Buscar
                                <i class="bi bi-search"></i> </button>
                    </form>

                </div>
            </div>
        </div>
        @error('error')
            <div class="text-center alert alert-danger"> {{ $message }}</div>
        @enderror

        <div class="row d-flex justify-content-center ">

            <div class="col-6 text-center">
                <p id="bold" class="text-light font-weight-bold">¡Que la lluvia no te pare!</p>

            </div>
        </div>
    </div>

    </div>
@endsection
