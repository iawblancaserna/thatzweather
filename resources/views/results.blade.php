@extends('layouts.header')
@section('content')
    <div class="container-fluid h-100">
        <div class="d-flex justify-content-center pt-5 pb-5">
            <img src="{{ URL::asset('/images/logo.png') }}" alt="logo thatz">
            <p class="text-light">¡Que la lluvia no te pare!</p>
        </div>
        <div class="d-flex flex-row m-3 text-light p-3">
            <div class="col-md-7 bg-blue m-2 p-3 shadow">
                <div class="row">
                    <div class="text-center col-md-7">
                        Código postal: <b>{{ $codi_postal }}</b><br>
                        Ciudad: <b>{{ $ciutat->name }}</b>
                    </div>
                    <div class="col-md-4">
                        <i class="float-right bi bi-search"></i>
                        <a class="text-info" href="/">Buscar otra zona</a>
                    </div>
                </div>
                <div class="row p-3 text-center"> 
                    <div class="col-md-3 p-3 ">  <br> <br><br><br><br><br>
                        {{-- Temperatura actual --}}
                        <p class='h4'> Ahora </p> <br>
                        <div class="row justify-content-left">
                            <div class="col-4">
                                <img src="http://openweathermap.org/img/w/{{ $ciutat->icon }}.png" alt="weather icon"
                                    width="80" height="80">
                            </div>
                            <div class="col-2">
                                <b class="text-capitalize"> {{ $ciutat->description }} </b>
                                <p class="text-center display-6"> {{ $ciutat->current_temp }}º </p>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-8 p-3 text-center" style="border-left: white 1px solid">
                        {{-- Temperatura de las proximas horas --}}
                        <p class='h5'> Próximas horas </p> <br>
                        <div class="row text-center">
                            @forelse ($proximes_hores as $hora)
                                <div class="col">
                                    {{ $hora['hora'] }} <br>
                                    <img src="http://openweathermap.org/img/w/{{ $hora['icon'] }}.png" alt="weather icon"
                                        width="75" height="75"> <br>
                                    <b class="text-capitalize"> {{ $hora['descripcion'] }} </b>
                                    <p class="text-center"> {{ $hora['temperatura_actual'] }}º </p>
                                </div>
                            @empty
                                None
                            @endforelse
                        </div> <br>
                        <div class="row" >
                            <p class='h5'> Próximos 5 días </p> <br> <br>

                            @forelse ($proxims_dies as $dia)
                                <div class="col">
                                    {{ $dia['dia'] }} <br>
                                    <img src="http://openweathermap.org/img/w/{{ $dia['icon'] }}.png" alt="weather icon"
                                        width="75" height="75"> <br>
                                    <b class="text-capitalize"> {{ $dia['descripcion'] }} </b>
                                    <p class="text-center"> {{ $dia['temperatura_actual'] }}º </p>
                                </div>
                            @empty
                                None
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 bg-blue m-2 shadow">
                {{-- Top 5 --}} <br>
                <div class="row p-3"> 
                    <p class='h5 text-center'> Top 5 de las zonas más frías según tus búsquedas </p> <br> <br><br> <br>
                    @php($num = 1)
                    @forelse ($ciutats_top as $item)
                        <div class="row">

                            <div class="col-2">
                                <p class="h5">{{ $num++ }}. </p>
                            </div>
                            <div class="col-3">
                                <p class="h3">{{ $item->current_temp }}º</p>
                            </div>
                            <div class="col">
                                <p>
                                    CP: <b>{{ $item->zip_code }}</b> <br>
                                    Ciudad: <b>{{ $item->name }}</b>
                                </p>
                            </div>
                        </div>
                        <hr>
                    @empty
                        0 entradas
                    @endforelse
                </div>

            </div>
        </div>
    </div>
@endsection
