<?php

use App\Http\Controllers\WeatherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// HOME
Route::get('/', function () {
    return view('home');
});

// Search weather
Route::get('/results', function () {
    return view('results');
});
Route::post('/results', [WeatherController::class, 'search'])->name('search.post');

